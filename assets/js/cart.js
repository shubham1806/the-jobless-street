function addItemToCart() {
    var item = {};
    var id = getSearchParams().id;
    item.id = id;
    item.quantity = 1;
    item.size = $("#sizeInput").val();
    if ($.cookie("cart") === undefined) {
        var cart = [item];
        $.cookie("cart", JSON.stringify(cart), { path: '/' });
        console.log($.parseJSON($.cookie("cart")));
    } else {
        var currentItems = $.parseJSON($.cookie("cart"));
        var modifiedcart;
        var flag = false;
        console.log(currentItems);

        $.each(currentItems, function(indexInArray, valueOfElement) {
            if ((valueOfElement.id === item.id) && (valueOfElement.size === item.size)) {
                currentItems[indexInArray].quantity = currentItems[indexInArray].quantity + 1;
                console.log(currentItems);
                $.cookie("cart", JSON.stringify(currentItems), { path: '/' });
                flag = true;
            } else {

            }
        });
        if (!flag) {
            currentItems.push(item);
            console.log(currentItems);
            $.cookie("cart", JSON.stringify(currentItems), { path: '/' });
        }

    }
    loadCartPlugin();
    loadCurrency();
    if ($("#cart")[0].style.display !== "block")
        $("#opencart").click();
    console.log(JSON.stringify($.cookie("cart")));
}

function removeItemFromCart(id, size) {
    var currentItems = $.parseJSON($.cookie("cart"));
    var newCart = [];
    console.log(currentItems.length, newCart)
    if (currentItems.length === 1) {
        console.log("flag")

    } else {
        $.each(currentItems, function(indexInArray, valueOfElement) {
            if (valueOfElement.id != id || valueOfElement.size != size) {
                newCart.push(valueOfElement);
            }
        })
    }
    console.log(newCart);
    if (newCart.length == 0) {
        $.cookie('cart', '', { expires: -1, path: '/' });
    } else {
        $.cookie("cart", JSON.stringify(newCart), { path: '/' });
    }
    loadCartPlugin();
}

function loadCartPlugin() {
    console.log("cart loaded");
    var setItems = $("#cart");
    var cartItemCount = $("#cartItemCount");
    console.log(setItems);
    console.log($.cookie("cart"));
    if ($.cookie("cart") === undefined || $.cookie("cart") === null) {
        // console.log("flag");
        cartItemCount.html(`0`);
        setItems.html(`
        <li>
                            <div class="cart-content">
                                <h3><a href="#"> No items</a> </h3>
                                
                            </div>
                        </li>
        `)
    } else {
        $.getJSON("/assets/json/products.min.json", function(data) {

            var setItems = $("#cart");
            var cartItemCount = $("#cartItemCount");
            var cart = $.parseJSON($.cookie("cart"));
            console.log(cart);
            cartItemCount.html(cart.length);
            setItems.html(``);
            var totalAmount = 0;
            for (var i = 0; i < cart.length; i++) {
                $.each(data, function(indexInArray, valueOfElement) {
                    if (valueOfElement.id == cart[i].id) {
                        var amount = valueOfElement.price * cart[i].quantity;
                        totalAmount += amount;
                        console.log(typeof cart[i].quantity, typeof valueOfElement.price, amount)
                        setItems.append(`
                            <li>
                                <div class="cart-img">
                                    
                                </div>
                                <div class="cart-content">
                                    <h3><a href="#"> ` + cart[i].quantity + ` X ` + valueOfElement.name + ` (` + cart[i].size + `)</a> </h3>
                                    <span class="cart-price"><i class="fas fa-rupee-sign"></i>  ` + valueOfElement.price + `</span>
                                </div>
                                <div class="cart-del" onclick="removeItemFromCart(` + cart[i].id + `, ` + cart[i].size + `)">
                                <i class="far fa-times-circle"></i>
                                </div>
                            </li>
                                `)
                    }
                })
            }
            setItems.append(`
            
            <li>
                            <div class="shipping">
                                <span class="f-left">Shipping </span>
                                <span class="f-right cart-price"> FREE</span>
                            </div>
                            <hr class="shipping-border" />
                            <div class="shipping">
                                <span class="f-left"> Total </span>
                                <span id="totalCartPrice" class="f-right cart-price"><i class="fas fa-rupee-sign"></i> ` + totalAmount + `</span>
                            </div>
                        </li>
            <li class="checkout m-0"><a href="/sections/shop/products/cart.html">View Cart <i class="fa fa-angle-right"></i></a></li>
            
            `);
        })
    }
}

function loadCartPage() {
    loadCartPlugin();
    console.log("Cart page loaded");
    if ($.cookie("cart") === undefined || $.cookie("cart") === null) {
        // console.log("flag");
        // cartItemCount.html(`0`);
        $("#cartContent").html(`
        No item in cart
        `)
        $("#subTotal").html(`NA`);
        $("#totalAmount").html(`NA`);
        $(".customerDetails").prop("disabled", true);
        $("#BtnPlaceOrder").hide();
        // $("#cartTotal").hide();
    } else {
        $.getJSON("/assets/json/products.min.json", function(data) {
            var cart = $.parseJSON($.cookie("cart"));
            console.log(cart);
            var subtotal = 0;
            $("#cartItems").html(``);
            for (var i = 0; i < cart.length; i++) {
                $.each(data, function(indexInArray, valueOfElement) {
                    if (valueOfElement.id == cart[i].id) {
                        var amount = valueOfElement.price * cart[i].quantity;
                        subtotal += amount;
                        $("#cartItems").append(`
                                <tr>
                                <td class="product-thumbnail">
                                    <a href="/sections/shop/products/product.html?id=` + cart[i].id + `"><img class="cart-thumb" src="` + valueOfElement.img.path + `" alt=""></a>
                                </td>
                                <td class="product-name">
                                    <a href="/sections/shop/products/product.html?id=` + cart[i].id + `">` + valueOfElement.name + `</a>
                                    <span>Size: ` + cart[i].size + `</span>
                                </td>
                                
                                <td class="product-price"><span class="amount"><i class="fas fa-rupee-sign"></i> ` + valueOfElement.price + `</span></td>
                                <td class="product-quantity">
                                    <div class="pro-details-quantity">
                                        <div class="pro-qty">
                                            <input type="number" value="` + cart[i].quantity + `" name="qtybutton" class="cart-plus-minus-box" disabled>
                                        </div>
                                    </div>
                                </td>
                                <td class="product-subtotal"><i class="fas fa-rupee-sign"></i> ` + amount + `</td>
                                <td class=""><a href="#" onclick="removeItemFromCart(` + cart[i].id + `, ` + cart[i].size + `);loadCartPage();"><i class="far fa-trash-alt"></i></a></td>
                            </tr>
                            `);
                    }

                })
            }
            $("#subTotal").html(`<i class="fas fa-rupee-sign"></i> ` + subtotal);
            $("#totalAmount").html(`<i class="fas fa-rupee-sign"></i> ` + subtotal);
        });
    }
}

function placeOrder() {
    

    // $.ajax({
    //     url : "/actions/placeOrder.php",
    //     type: "POST",
    //     data : {
    //      name: name,
    //      email: email,
    //      phone: phone,
    //      address: address  
    //     },
    //     processData: false,
    // contentType: false,
    //     success: function(data, textStatus, jqXHR)
    //     {
    //         console.log(data);
    //     },
    //     error: function (jqXHR, textStatus, errorThrown)
    //     {
     
    //     }
    // });
    $.getJSON("/assets/json/products.min.json", function(data) {
            var orderStatus = $("#orderStatus");
            var BtnPlaceOrder = $("#BtnPlaceOrder");
            
            var name = $("#name").val();
            var email = $("#email").val();
            var mobile = $("#mobile").val();
            var address = $("#address").val();

            if(name === '' || email === '' || mobile === '' || address === '') {
                alert("Please enter all the details mentioned");
            } else {
                BtnPlaceOrder.hide();
            orderStatus.html(`<center><i class="fas fa-spinner fa-spin"></i> Placing Order</center>`);
            orderStatus.show();

                var cookie = $.cookie("cart");
            console.log($("#cartItems>tr"));
            var cart = $.parseJSON($.cookie("cart"));
            var currency = $.cookie("currency");
            console.log(cart);
            var subtotal = 0;
            var order = [];
            var key=0;

            for (var i = 0; i < cart.length; i++) {
                $.each(data, function(indexInArray, valueOfElement) {
                    if (valueOfElement.id == cart[i].id) {
                        var product = valueOfElement;
                        var amount = product.price * cart[i].quantity;
                        subtotal += amount;
                        order[key] = {
                            id: product.id,
                            name: product.name,
                            img: product.img.path,
                            size: cart[i].size,
                            price: product.price,
                            quantity: cart[i].quantity,
                            total: amount
                        }
                        key++;
                    }
                })
            }
            console.log(order);
            $.post("/actions/placeOrder.php",
            {
                name,
                email,
                mobile,
                address,
                order,
                currency,
                subtotal
            }).done(function(data, textStatus, jqXHR) 
            {
                console.log("data: ", data);
                if(data == 1) {
                    orderStatus.html(`<center style="color: green"><i class="fas fa-check"></i> Order Placed. <br>Thanks for shopping with us.</center>`);
                }
                }).fail(function(jqXHR, textStatus, errorThrown) 
                {
                    console.log(textStatus, errorThrown);
                });
        
            }

            
        });
    
    

}