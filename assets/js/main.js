(function($) {
    "use strict";



    /* cart */
    $(".icon-cart").on("click", function() {
        $(this).parent().find('.cart-toogle').slideToggle('medium');
    })

    /* search */
    $(".icon-search").on("click", function() {
        $(this).parent().find('.toogle-content').slideToggle('medium');
    })


    $('.slider-active').owlCarousel({
        loop: true,
        nav: true,
        items: 1,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        navText: ['<i class="fa fa-long-arrow-left"></i>', '<i class="fa fa-long-arrow-right"></i>'],
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    })


    $('.slider-active-5').owlCarousel({
        loop: true,
        nav: true,
        items: 1,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        navText: ['<i class="fa fa-long-arrow-left"></i>', '<i class="fa fa-long-arrow-right"></i>'],
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    })


    $('.brand-logo-active').owlCarousel({
        loop: true,
        nav: true,
        margin: 30,
        items: 5,
        navText: ['<i class="fa fa-long-arrow-left"></i>', '<i class="fa fa-long-arrow-right"></i>'],
        responsive: {
            0: {
                items: 2
            },
            768: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    })


    $('.single-portfolio-slider-active').owlCarousel({
        loop: true,
        nav: true,
        margin: 30,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        items: 1,
        navText: ['<i class="fa fa-long-arrow-left"></i>', '<i class="fa fa-long-arrow-right"></i>'],
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    })





    $('.testimonials-active').owlCarousel({
        loop: true,
        nav: false,
        margin: 30,
        items: 2,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 1
            },
            1000: {
                items: 2
            }
        }
    })


    $('.slider-active-seven').owlCarousel({
        loop: true,
        autoplay: true,
        autoplayTimeout: 5000,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        nav: false,
        items: 1,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    })


    $('.testimonials-active-4').owlCarousel({
        loop: true,
        nav: false,
        items: 3,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    })


    $('.blog-slider-active').owlCarousel({
        loop: true,
        nav: true,
        navText: ['<i class="fa fa-long-arrow-left"></i>', '<i class="fa fa-long-arrow-right"></i>'],
        items: 1,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    })

    $('.sidebar-slider').owlCarousel({
        loop: true,
        nav: false,
        items: 1,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    })


    $('.testimonials-3-active').owlCarousel({
        loop: true,
        nav: false,
        items: 1,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    })


    $('.details-tab').owlCarousel({
        loop: true,
        nav: false,
        margin: 30,
        items: 5,
        responsive: {
            0: {
                items: 2
            },
            768: {
                items: 4
            },
            1000: {
                items: 5
            }
        }
    })


    /* isotop active */
    // filter items on button click

    var portfolioarea = $('.portfolio-area , .portfolio-style');
    var portfoliomenuactive = $('.portfolio-menu-active');
    portfolioarea.imagesLoaded(function() {
        portfoliomenuactive.on('click', 'button', function() {
            var filterValue = $(this).attr('data-filter');
            $grid.isotope({
                filter: filterValue
            });
        });
        // init Isotope
        var $grid = $('.grid').isotope({
            itemSelector: '.grid-item',
            percentPosition: true,
            masonry: {
                // use outer width of grid-sizer for columnWidth
                columnWidth: '.grid-item',
            }
        });
    });


    /* isotop active */
    // filter items on button click
    portfolioarea.imagesLoaded(function() {
        portfoliomenuactive.on('click', 'button', function() {
            var filterValue = $(this).attr('data-filter');
            $grid.isotope({
                filter: filterValue
            });
        });
        // init Isotope
        var $grid = $('.grid-2').isotope({
            itemSelector: '.grid-item-2',
            percentPosition: true,
            masonry: {
                // use outer width of grid-sizer for columnWidth
                columnWidth: 1,
            }
        });
    });


    $('.portfolio-menu-active button').on('click', function(event) {
        $(this).siblings('.active').removeClass('active');
        $(this).addClass('active');
        event.preventDefault();
    });


    /* magnificPopup image popup */
    $('.img-poppu').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        }
    });


    /*--
    Testimonial Slick Carousel
    -----------------------------------*/
    $('.testimonial-text-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        draggable: false,
        fade: true,
        asNavFor: '.slider-nav',
    });


    /*--
        Testimonial Slick Carousel as Nav
    -----------------------------------*/
    $('.testimonial-image-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.testimonial-text-slider',
        dots: false,
        arrows: false,
        centerMode: true,
        focusOnSelect: true,
        centerPadding: '0px',
        responsive: [{
                breakpoint: 767,
                settings: {
                    dots: false,
                    slidesToShow: 1,
                }
            },
            {
                breakpoint: 420,
                settings: {
                    autoplay: true,
                    dots: false,
                    slidesToShow: 2,
                    centerMode: false,
                }
            }
        ]
    });


    /*---------------------
    price slider
    --------------------- */
    var sliderrange = $('#slider-range');
    var amountprice = $('#amount');
    $(function() {
        sliderrange.slider({
            range: true,
            min: 0,
            max: 1200,
            values: [0, 1000],
            slide: function(event, ui) {
                amountprice.val("$" + ui.values[0] + " - $" + ui.values[1]);
            }
        });
        amountprice.val("$" + sliderrange.slider("values", 0) +
            " - $" + sliderrange.slider("values", 1));
    });


    /*----- 
    	Cart Plus Minus
    --------------------------------*/
    $('.pro-qty, .cart-plus-minus-2').append('<span class="inc qtybtn"><i class="fa fa-plus"></i></span>');
    $('.pro-qty, .cart-plus-minus-2').append('<span class="dec qtybtn"><i class="fa fa-minus"></i></span>');
    $('.qtybtn').on('click', function() {
        var $button = $(this);
        var oldValue = $button.parent().find('input').val();
        if ($button.hasClass('inc')) {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below zero
            if (oldValue > 0) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }
        $button.parent().find('input').val(newVal);
    });


    /*--- showlogin toggle function ----*/
    $('#showlogin').on('click', function() {
        $('#checkout-login').slideToggle(900);
    });


    /*--- showlogin toggle function ----*/
    $('#showcoupon').on('click', function() {
        $('#checkout_coupon').slideToggle(900);
    });


    /*--- showlogin toggle function ----*/
    $('#ship-box').on('click', function() {
        $('#ship-box-info').slideToggle(1000);
    });


    /* parallax active  */
    $('.parallax-window').parallax();


    /*--------------------------
        09. ScrollUp
    ---------------------------- */
    $.scrollUp({
        scrollText: '<i class="fa fa-angle-double-up"></i>',
        easingType: 'linear',
        scrollSpeed: 900,
        animation: 'fade'
    });


    /* jQuery MeanMenu */
    $('#mobile-menu-active').meanmenu({
        meanScreenWidth: "767",
        meanMenuContainer: ".mobile-menu-area .mobile-menu",
    });


    /*------------------------------------    
      08. Toogle Menu
    --------------------------------------*/
    var bodyoverlay = $('.body__overlay');
    var offsetmenu = $('.offsetmenu');
    $('.toggle__menu').on('click', function() {
        offsetmenu.addClass('offsetmenu__on');
        bodyoverlay.addClass('is-visible');
    });

    $('.offsetmenu__close__btn').on('click', function() {
        offsetmenu.removeClass('offsetmenu__on');
        bodyoverlay.removeClass('is-visible');
    });


    /*------------------------------------    
      09. Overlay Close
    --------------------------------------*/
    bodyoverlay.on('click', function() {
        $(this).removeClass('is-visible');
        offsetmenu.removeClass('offsetmenu__on');
    });


    /* creative-menu-6 */
    $(".sidebar-menu-active").on("click", function() {
        $(".sidebar-mega-menu").toggleClass('open');
        return false;
    })


    /*--
    Menu Stick
    -----------------------------------*/
    var header = $('.transparent-bar');
    var win = $(window);

    win.on('scroll', function() {
        var scroll = win.scrollTop();
        if (scroll < 1) {
            header.removeClass('stick');
        } else {
            header.addClass('stick');
        }
    });

    var sorting = $('.sorting-bar');
    var win = $(window);

    win.on('scroll', function() {
        var scroll = win.scrollTop();
        if (scroll < 103) {
            sorting.removeClass('stick');
            $(".product-bottom-area").removeClass("sticky_header_catalog");
        } else {
            sorting.addClass('stick');
            $(".product-bottom-area").addClass("sticky_header_catalog");
        }
    });

    /* counterUp */
    $('.counter').counterUp({
        delay: 10,
        time: 1000
    });

    /* magnific video popup */
    $('.video-popup').magnificPopup({
        type: 'iframe'
    });


    // header Initialization
    $.getJSON("/assets/json/metadata.min.json",
        function(metadata) {
            console.log(metadata);
            $(".logo_header_headline").attr("href", metadata.links.home);
            $("a:contains('Home')").attr("href", metadata.links.home);
            $("a:contains('About')").attr("href", metadata.links.about);
            $("a:contains('Shop')").attr("href", metadata.links.shop);
            // $("a:contains('Blog')").attr("href", metadata.links.blog);
            $("a:contains('Contact')").attr("href", metadata.links.contact);
            // $("a:contains('Designer')").attr("href", metadata.links.designerPortfolio);

            /* Social Media Icons */
            $("a i.fab.fa-facebook-f").parent().attr("href",metadata.links.fb);
            $("a i.fab.fa-instagram").parent().attr("href",metadata.links.insta);
            $("a i.fab.fa-twitter").parent().attr("href",metadata.links.twitter);
            $("a i.fab.fa-tumblr").parent().attr("href",metadata.links.tumblr);


            $("#currencySelect").html('');
            $.each(metadata.allowedCurrency, function(indexInArray, valueOfElement) {
                $("#currencySelect").append(`<option value=`+valueOfElement.symbol+`>`+valueOfElement.code+`</option>`)
            });
            $("#currencySelect").on("change", changeCurrency);

        });

        
        

})(jQuery);

function changeCurrencySymbol() {
    const selectedCurrency = $("#currencySelect")[0].selectedOptions[0];
            $.getJSON("https://api.ratesapi.io/api/latest?base="+selectedCurrency.innerText+"&symbols=INR",
                function(data) {
                            $("#productPrice").html('<i class="fas fa-'+selectedCurrency.value+'-sign"></i> ' 
                + parseFloat(product.price / data.rates["INR"]).toFixed(2));                 
                   
                })

}

function loadCurrency() {
    console.log("currency loaded");

    var currency = $.cookie("currency");
    var currencyOptionSelect = $("#currencySelect option:contains("+currency+")");
    console.log(currencyOptionSelect);

    if(currency === undefined || currency === null) {
        $.cookie("currency","INR", { path: '/' });
    } else {
        currencyOptionSelect.attr("selected","selected");
    }
    console.log("https://api.ratesapi.io/api/latest?base="+currency+"&symbols=INR");
        $.getJSON("https://api.ratesapi.io/api/latest?base="+currency+"&symbols=INR",
                function(data) {
                    // Price Update for Browse page
                    for(var i = 0; i < $("#catalog .product-price").length; i++) {
                        $("#catalog .product-price")[i].innerHTML = `
                        <i class="fas fa-`+currencyOptionSelect.val()+`-sign"></i> ` 
                            + parseFloat($("#catalog .product-price")[i].innerText / data.rates["INR"]).toFixed(2);
                            console.log($("#catalog .product-price")[i].innerHTML);
                    }
                    // Price Update for Product page
                    if($("#productPrice").length > 0) {
                        $("#productPrice").html('<i class="fas fa-'+currencyOptionSelect.val()+'-sign"></i> ' 
                        + parseFloat($("#productPrice")[0].innerText / data.rates["INR"]).toFixed(2));
                    }
                        
                    // Price Update for Cart products in widget
                    for(var i = 0; i < $(".cart-content .cart-price").length; i++) {
                        $(".cart-content .cart-price")[i].innerHTML = `
                        <i class="fas fa-`+currencyOptionSelect.val()+`-sign"></i> ` 
                            + parseFloat($(".cart-content .cart-price")[i].innerText / data.rates["INR"]).toFixed(2)
                    }
                    if($("#totalCartPrice").length > 0) {
                        $("#totalCartPrice").html('<i class="fas fa-'+currencyOptionSelect.val()+'-sign"></i> ' 
                        + parseFloat($("#totalCartPrice")[0].innerText / data.rates["INR"]).toFixed(2));
                    }

                    // Price Update for Cart products in cart page
                    for(var i = 0; i < $("#cartItems .product-price .amount").length; i++) {
                        $("#cartItems .product-price .amount")[i].innerHTML = `
                        <i class="fas fa-`+currencyOptionSelect.val()+`-sign"></i> ` 
                            + parseFloat($("#cartItems .product-price .amount")[i].innerText / data.rates["INR"]).toFixed(2)
                        
                        $("#cartItems .product-subtotal")[i].innerHTML = `
                        <i class="fas fa-`+currencyOptionSelect.val()+`-sign"></i> ` 
                            + parseFloat($("#cartItems .product-subtotal")[i].innerText / data.rates["INR"]).toFixed(2)
                    }
                    if($(".cart-total #subTotal").length > 0) {
                        $(".cart-total #subTotal").html('<i class="fas fa-'+currencyOptionSelect.val()+'-sign"></i> ' 
                        + parseFloat($(".cart-total #subTotal")[0].innerText / data.rates["INR"]).toFixed(2));
                    }
                    if($(".cart-total #totalAmount").length > 0) {
                        $(".cart-total #totalAmount").html('<i class="fas fa-'+currencyOptionSelect.val()+'-sign"></i> ' 
                        + parseFloat($(".cart-total #totalAmount")[0].innerText / data.rates["INR"]).toFixed(2));
                    }


                })
}

function changeCurrency() {
    var selectedCurrency = $("#currencySelect")[0].selectedOptions[0].innerText;
    console.log("Currency Changed: "+selectedCurrency);

    var currency = $.cookie("currency");
    if(currency === undefined || currency === null) {
        $.cookie("currency","INR", { path: '/' });
    }

    //changeCurrencySymbol();
    loadCurrency();

    $.cookie("currency",selectedCurrency, { path: '/' });

    location.reload();
}