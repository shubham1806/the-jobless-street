var loadBrowsePage = function(page = 1, priceSort = false) {
    $("#catalog").html(`<center><i class="fas fa-spinner fa-spin"></i> Loading Products</center>`);
    $.getJSON("/assets/json/products.min.json",
        function(data) {

            data.sort((element) => {
                return element.available ? -1 : 1
            });

            var start = (9 * (page - 1));


            var totalProducts = data.length;
            var totalPages = Math.ceil(data.length / 9);
            console.log(totalProducts - start);
            if ((totalProducts - start) >= 9) {
                var end = (9 * (page - 1)) + 8;
            } else {
                end = totalProducts - 1;
            }
            console.log(page, start, end);
            console.log("Total Pages:", totalPages);
            var currentProducts = 0;
            if (totalProducts <= 9) {
                $("#browseSummary").text(`Showing 1-` + totalProducts + ` of ` + totalProducts);
                $(".paginations").hide();
                currentProducts = totalProducts;
            } else {
                $("#browseSummary").text(`Showing ` + (start + 1) + `-` + (end + 1) + ` of ` + totalProducts);
                $(".paginations").show();
                $("#pagination_pages").html(``);
                if (page > 1)
                    $("#pagination_pages").html(`<li><a href="#" onclick="loadBrowsePage(1, ` + priceSort + `)"><i class="fa fa-angle-left"></i></a></li>`);
                for (var i = 1; i <= totalPages; i++) {
                    if (page == i) {
                        $("#pagination_pages").append(`<li class="active"><a href="#"  onclick="loadBrowsePage(` + i + `, ` + priceSort + `)">` + i + `</a></li>`);
                    } else {
                        $("#pagination_pages").append(`<li><a href="#" onclick="loadBrowsePage(` + i + `, ` + priceSort + `)">` + i + `</a></li>`);
                    }

                }
                if (page != totalPages)
                    $("#pagination_pages").append(`<li><a href="#" onclick="loadBrowsePage(` + totalPages + `, ` + priceSort + `)" ><i class="fa fa-angle-right"></i></a></li>`);
                currentProducts = 9;
            }
            console.log("Price Sort:", priceSort);
            var sortByPrice = (property) => {
                return (a, b) => {
                    // console.log("in sortByPrice method")
                    if (a[property] > b[property]) {
                        return 1;
                    } else if (a[property] < b[property]) {
                        return -1;
                    } else {
                        return 0;
                    }
                }
            }
            if (priceSort) {
                console.log("sorting price");
                data.sort(sortByPrice("price"));
                console.log(data);
            }
            console.log("total Products:", totalProducts);
            $("#catalog").html('');

            for (var i = start; i <= end; i++) {
                var outOfStockBanner = ``;
                if(!data[i].available) {
                    outOfStockBanner = `
                    <span class="outOfStockBanner">Out of Stock</span>
                    `
                }
                $("#catalog").append(
                    `
                        <div class="col-md-4 col-sm-6 catelog_product">
                            <div class="single-product mb-10">
                                <div class="product-img">
                                `+outOfStockBanner+`
                                    <a href="/sections/shop/products/product.html?id=` + data[i].id + `">
                                        <img src="` + data[i].img.path + `" alt="">
                                    </a>
                                    <div class="product-icon text-center">
                                        <a href="/sections/shop/products/product.html?id=` + data[i].id + `">
                                            <img src="` + data[i].img.wrapperPath + `" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="product-title text-center">
                                    <h3><a href="/sections/shop/products/product.html?id=` + data[i].id + `">` + data[i].name + `</a></h3>
                                        <span class="product-price"><i class="fas fa-rupee-sign"></i> ` + data[i].price + `</span>
                                </div>
                            </div>
                        </div>
                `
                );
            }
        }
    );
}


function getSearchParams(k) {
    var p = {};
    window.location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(s, k, v) {
        p[k] = v
    })
    return k ? p[k] : p;
}

function fetchProduct(id) {
    var result;
    $.getJSON("/assets/json/products.min.json",
        function(data) {
            $.each(data, function(indexInArray, valueOfElement) {
                if (valueOfElement.id == id) {
                    console.log(valueOfElement, indexInArray, true);
                    result = valueOfElement;
                    console.log(result);
                    $.cookie("product", JSON.stringify(valueOfElement));
                }
            });

        }
    );

}

async function loadproduct() {
    $("#productTitle").html('');
    $("#productPrice").html('');
    $("#productDescription").html('');
    console.log(getSearchParams());
    params = getSearchParams();
    // $("#productTab1")[0].innerHTML = ``;
    $("#relatedItems").html('');
    $.getJSON("/assets/json/products.min.json",
        function(data) {
            var totalProducts = data.length;
            $.each(data, function(indexInArray, valueOfElement) {
                if (valueOfElement.id == params.id) {
                    product = valueOfElement;
                    console.log(product);
                    // basic details
                    $("#breadcrumb").append(`<li>` + product.name + `</li>`)
                    $("#productTitle").html(product.name);
                    changeCurrencySymbol();
                    
                    // $("#productDescription").html(product.description);
                    // images
                    $("#productTab1")[0].innerHTML = `<img id="img1" class="zoom" src="` + product.img.path + `" alt="" />`;
                    $("#img2").attr('src', product.img.additionalImages[0]);
                    $("#img3").attr('src', product.img.additionalImages[2]);

                    $(".img1").attr('src', product.img.path);
                    $(".img2").attr('src', product.img.additionalImages[0]);
                    $(".img3").attr('src', product.img.additionalImages[2]);
                    // product_id
                    $("#pr_id").html('TJS' + product.id)
                        // rating
                    $('#rating').html(``);
                    for (var i = 1; i <= product.rating; i++) {
                        $('#rating').append(`<i class="fa fa-star"></i>`);
                    }
                    for (var i = 1; i <= (5 - product.rating); i++) {
                        $('#rating').append(`<span class="light-color"><i class="fa fa-star"></i></span>`);
                    }

                    // availability
                    if (product.available) {
                        $("#availability").html(`In Stock`)
                    } else {
                        $("#addToCartBtn").hide();
                        $("#availability").html(`<span style="color:red">Out of Stock</span>`)
                    }
                    // size
                    $("#sizeInput").html(``);
                    console.log(product.availableSizes)
                    for (var i = 0; i < product.availableSizes.length; i++) {
                        $("#sizeInput").append(`<option value="` + product.availableSizes[i] + `">` + product.availableSizes[i] + `</option>`)
                    }

                    // colors
                    // $("#colors").html(``);
                    // for (var i = 0; i < product.colors.length; i++) {
                    //     $("#colors").append(`<li style="background: ` + product.colors[i] + ` none repeat scroll 0 0;"></li>`)
                    // }

                    // Description_images
                    console.log(product.img.additionalImages);
                    $("#description_feature_img1").html(`<img class="product_description_image" src="` + product.img.wrapperPath + `" alt="" style="float:right;    width: 100%;" />`);
                    $("#description_feature_img2").html(`<img class="product_description_image" src="` + product.img.additionalImages[0] + `" alt="" style="float:left; width: 100%;" />`);
                    $("#description_feature_img3").html(`<img class="product_description_image" src="` + product.img.additionalImages[1] + `" alt="" style="float:left; width: 100%;" />`);
                    $("#description_feature_img4").html(`<img class="product_description_image" src="` + product.img.additionalImages[2] + `" alt="" style="width:100%" />`);
                    // Description
                    $("#description_text").html(`<p>`+product.description+`</p>`);
                    // reviews
                    // $("#reviewsSection").html(``);
                    // for (var i = 0; i < product.reviews.length; i++) {
                    //     $("#reviewsSection").append(`
                    //     <div class="sin-rattings">
                    //             <div class="star-author-all">
                    //                 <div class="ratting-star f-left" id="reviewsRating` + i + `">
                    //             <span>(` + product.reviews[i].rating + `)</span>
                    //                 </div>
                    //                 <div class="ratting-author f-right">
                    //                     <h3>` + product.reviews[i].name + `</h3>
                    //                     <span>` + product.reviews[i].time + `</span>
                    //                     <span>` + product.reviews[i].date + `</span>
                    //                 </div>
                    //             </div>
                    //             <p>` + product.reviews[i].message + `</p>
                    //         </div>


                    //     `);
                    //     console.log("rating", product.reviews[i].rating);
                    //     for (var j = 0; j < product.reviews[i].rating; j++) {
                    //         $("#reviewsRating" + i).prepend(`
                    //         <i class="fa fa-star"></i>
                    //         `);
                    //     }
                    //     $("#reviewsSection").append(`
                        
                    //         `);
                    // }

                    $.cookie("product", JSON.stringify(valueOfElement));
                } else if(valueOfElement.available){
                    $("#relatedItems").append(`
                    <div class="col-md-3">
                        <div class="single-product mb-10">
                            <div class="related-img">
                                <a href="/sections/shop/products/product.html?id=` + valueOfElement.id + `">
                                    <img src="` + valueOfElement.img.path + `" alt="">
                                </a>
                            </div>
                            <div class="product-title text-center">
                                <h3><a href="/sections/shop/products/product.html?id=` + valueOfElement.id + `">` + valueOfElement.name + `</a></h3>
                                <span>&#8377;` + valueOfElement.price + `</span>
                            </div>
                        </div>
                    </div>
                    `);
                }

            });

        }

    );
    setTimeout(function() {
        $('.related-post-active').owlCarousel({
            loop: true,
            nav: true,
            items: 4,
            navText: ['<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>'],
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 3
                },
                1000: {
                    items: 4
                }
            }
        })
    }, 50)




    // $(fetchProduct(params.id)).promise().done(function() {
    //     var product = JSON.parse($.cookie("product"))
    //     if (params.id != product.id) {
    //         $(fetchProduct(params.id)).promise().done(function() {
    //             var product = JSON.parse($.cookie("product"));
    //             console.log(product);
    //             $("#productTitle").html(product.name);
    //             $("#productPrice").html('&#8377;' + product.price);
    //             $("#productDescription").html(product.description);
    //         });
    //     } else {
    //         console.log(product);
    //         $("#productTitle").html(product.name);
    //         $("#productPrice").html('&#8377;' + product.price);
    //         $("#productDescription").html(product.description);
    //     }
    // });
    // // $.cookie("product", null);

}