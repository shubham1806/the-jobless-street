<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require './PHPMailer/src/Exception.php';
require './PHPMailer/src/PHPMailer.php';
require './PHPMailer/src/SMTP.php';

date_default_timezone_set('Asia/Kolkata');
$name=$_POST["name"];
$email=$_POST["email"];
$mobile=$_POST["mobile"];
$address = $_POST["address"];
$order = $_POST["order"];
$subtotal = $_POST["subtotal"];
$currency = $_POST["currency"];

$ratesAPI = json_decode(file_get_contents("https://api.ratesapi.io/api/latest?base=".$currency."&symbols=INR"), true);
$currentRate = $ratesAPI["rates"]["INR"];

$message = '<!DOCTYPE html>
         <html lang="en">
         <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
           <title>Bootstrap Example</title>
           
           <meta name="viewport" content="width=device-width, initial-scale=1">
           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
           <style>
        * {
            font-family: Arial, Helvetica, sans-serif;
        }

        table {
            border-collapse: collapse;
        }

        @media only screen and (min-width: 1200px) {
            .logo_header_headline {
                font-size: 24px;
                padding-left: 0;
            }
        }

        @media only screen and (min-width: 700px) {
            .logo_header_headline {
                font-size: 16px;
                padding-left: 0;
            }
        }

        .logo_header_headline {
            font-family: "Playfair Display", serif;
            color: #000 !important;
            font-size: 32px;
            text-transform: uppercase;
            text-decoration: none;
        }

        @media only screen and (min-width: 1200px) {
            .logo_header {
                width: 20%;
            }
        }

        @media only screen and (min-width: 700px) {
            .logo_header {
                width: 20%;
            }
        }

        @media only screen and (min-width: 370px) {
            .logo_header {
                width: 10%;
            }
        }

        @media only screen and (min-width: 300px) {
            .logo_header {
                width: 10%;

            }
        }

        .logo_header {
            width: 8%;

        }
        img.product_image {
          width: 100px;
          height: 100px;
        }
        table th,table td {
          min-width: 100px;
        }
        table td {
          text-align: center;
        }
    </style>
         </head>
         <body>
         <center>
         <a href="https://www.thejoblessstreet.com/" style="" class="logo_header_headline cart_header">
            <img src="https://www.thejoblessstreet.com/assets/img/logo/logo_black.png" class="logo_header" alt=""> The Jobless Street
         </a>
            <h2>Order Placed</h2>
         </center>
         <hr>
         <h4>Date: '.date("d/M/Y").'</h4>
         <h4>Time: '.date("h:ia").'</h4>
         <hr>
             <h4>Name: '.$name.'</h4>
             <h4>Email: '.$email.'</h4>
             <h4>Phone: '.$mobile.'</h4>
             <h4>Address: '.$address.'</h4>
             <hr>
         <div class="container">
           <h2>Order Details</h2>
           <p>Please find the order details below:</p>            
           <table border="1" class="table">
             <thead>
               <tr>
                 <th>S.No</th>
                 <th>Id</th>
                 <th>Product Image</th>
                 <th>Product Name</th>
                 <th>size</th>
                 <th>Price</th>
                 <th>Quantity</th>
                 <th>Total</th>
               </tr>
             </thead>
             <tbody>';
             for($i=0;$i < count($order); $i++) {
                 $ratePrice = number_format(((float)$order[$i]["price"] / (float)$ratesAPI["rates"]["INR"]), 2, '.', '');
                 $rateTotal = number_format(((float)$order[$i]["total"] / (float)$ratesAPI["rates"]["INR"]), 2, '.', '');
               $message .= '
               <tr>
                <td>'.($i+1).'</td>
                <td>'.$order[$i]["id"].'</td>
                <td><img class="product_image" src="https://www.thejoblessstreet.com'.$order[$i]["img"].'" ></td>
                <td>'.$order[$i]["name"].'</td>
                <td>'.$order[$i]["size"].'</td>
                <td>'.$currency." ".$ratePrice.'</td>
                <td>'.$order[$i]["quantity"].'</td>
                <td>'.$currency.' '.$rateTotal.'</td>
              </tr>';
             }
             $subTotalCurrencyFixed = number_format(((float)$subtotal / (float)$ratesAPI["rates"]["INR"]), 2, '.', '');
             $message .= '
              </tbody>
              </table>

              <h2>Order Total</h2>
              <h3> Sub total:'.$currency.' '.$subTotalCurrencyFixed.'
              <h4> Sub total (in INR):Rs. '.$subtotal.'
            </div>
            </body>
            </html>
             ';

$fp = fopen('errorLog.txt', 'a');

// Instantiation and passing [ICODE]true[/ICODE] enables exceptions
$mail = new PHPMailer(true);

try {
 //Server settings
 $mail->isSMTP(); // Set mailer to use SMTP
 $mail->Host = 'mail.thejoblessstreet.com'; // Specify main and backup SMTP servers
 $mail->SMTPAuth = true; // Enable SMTP authentication
 $mail->Username = 'orders@thejoblessstreet.com'; // SMTP username
 $mail->Password = 'orders@123456'; // SMTP password
 $mail->SMTPSecure = 'tls'; // Enable TLS encryption, [ICODE]ssl[/ICODE] also accepted
 $mail->Port = 587; // TCP port to connect to

//Recipients
 $mail->setFrom('orders@thejoblessstreet.com', 'TJS Orders');
 $mail->addAddress('saikirangaddam001@gmail.com', 'Sai Kiran Gaddam');
 $mail->addAddress('ssharma4shubham@gmail.com', 'Shubham Sharma'); // Add a recipient
 $mail->addReplyTo('info@thejoblessstreet.com', 'Information');

// Attachments

// Content
 $mail->isHTML(true); // Set email format to HTML
 $mail->Subject = 'New Order placed on TJS';
 $mail->Body = $message;
 $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

$mail->send();
             fwrite($fp, 'Email Sent Successfully. The message is shown below:'.PHP_EOL);
             fwrite($fp,$message);
 echo true;

} catch (Exception $e) {
 fwrite($fp, "Message could not be sent. Mailer Error: {$mail->ErrorInfo}");
 echo false;
}
fclose($fp);
      ?>